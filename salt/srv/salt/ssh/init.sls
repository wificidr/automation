install_openssh:
  pkg.installed:
    - name: openssh-server

start_sshd:
  service.running:
    - name: sshd
    - enable: True

sshkeys:
  ssh_auth.manage:
    - user: djustice
    - comment: Managed by Salt
    - enc: ed25519
    - ssh_keys:
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICuuaH1WaSj4XEeXU2H5pwQrr8T3K5cqEkeGSTIN5lNB djustice@wificidr.net
