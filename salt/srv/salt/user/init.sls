docker:
  group.present:
    - system: True

djustice:
  user.present:
    - fullname: Daniel Justice
    - home: /home/djustice
    - groups:
      - sudo
      - docker
    - password: "$5$Y6vgXrmSIpXfwyQu$k6mCL2HlOnm.E9rviXw8bExGsqERBgwCCIfuQvfzY71"
