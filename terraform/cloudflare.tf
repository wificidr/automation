data "vault_generic_secret" "cf_credentials" {
  path = "secret/cloudflare"
}

data "vault_generic_secret" "tun_test_tickle" {
  path = "secret/cloudflare/tunnels/test-tickle"
}

terraform {
  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~> 4"
    }
  }
}

provider "cloudflare" {
  api_token = data.vault_generic_secret.cf_credentials.data["api_token"]
}

variable "domain" {
  default = "wificidr.net"
}

resource "cloudflare_tunnel" "example" {
  account_id  = data.vault_generic_secret.cf_credentials.data["account_id"]
  name       = "test-tickle"
  secret  = data.vault_generic_secret.tun_test_tickle.data["secret"]
  config_src = "cloudflare"
}

