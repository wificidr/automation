/*Provider bindings are generated by running cdktf get.
See https://cdk.tf/provider-generation for more details.*/
import * as netbox from "./.gen/providers/netbox";

import { Construct } from "constructs";
import { App, TerraformStack } from "cdktf";

class NetboxProvider extends Construct {
  constructor(scope: Construct, id: string) {
    super(scope, id);

    new netbox.provider.NetboxProvider(this, "netbox", {
      apiToken: process.env.NETBOX_API_TOKEN!,
      serverUrl: "http://192.168.1.155:8000",
    });
  }
}

class MyStack extends TerraformStack {
  constructor(scope: Construct, id: string) {
    super(scope, id);

    new NetboxProvider(this, "netbox");

    // regions
    const northAmerica = new netbox.region.Region(this, "north-america", {
      name: "North America",
    });

    // sites
    const arborAcres = new netbox.site.Site(this, "arbor-acres", {
      facility: "Office",
      latitude: 36.1758,
      longitude: -94.2318,
      name: "Arbor Acres",
      status: "active",
      timezone: "US/Central",
      regionId: Number(northAmerica.id),
    });

    // racks
    new netbox.rack.Rack(this, "central-office", {
      siteId: Number(arborAcres.getNumberAttribute("id")),
      status: "active",
      name: "Central Office",
      uHeight: 42,
      width: 19,
    });
    new netbox.rack.Rack(this, "green-room", {
      siteId: Number(arborAcres.getNumberAttribute("id")),
      status: "active",
      name: "Green Room",
      uHeight: 42,
      width: 19,
    });

    const linksysDeviceType =
      new netbox.dataNetboxDeviceType.DataNetboxDeviceType(
        this,
        "wrt1200acDeviceType",
        { model: "WRT1200AC" }
      );
    const routerDeviceRole =
      new netbox.dataNetboxDeviceRole.DataNetboxDeviceRole(
        this,
        "routerDeviceRole",
        { name: "Router" }
      );
    // devices
    new netbox.device.Device(this, "edge01.arboracres", {
      name: "edge01.arboracres",
      deviceTypeId: linksysDeviceType.getNumberAttribute("id"),
      roleId: routerDeviceRole.getNumberAttribute("id"),
      siteId: arborAcres.getNumberAttribute("id"),
    });
  }
}

const app = new App();
new MyStack(app, "cdktf");
app.synth();
